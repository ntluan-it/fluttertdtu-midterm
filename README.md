# Flutter Keep

## Libraries 

- Provider : https://pub.dev/packages/provider
- Flutter Staggared Grid View : https://pub.dev/packages/flutter_staggered_grid_view
- Hive : https://pub.dev/packages/hive
- Hive Flutter : https://pub.dev/packages/hive_flutter
- Hive Generator : https://pub.dev/packages/hive_generator
- Build Runner : https://pub.dev/packages/build_runner
- Google Fonts : https://pub.dev/packages/google_fonts
- Intl : https://pub.dev/packages/intl
- Uuid : https://pub.dev/packages/uuid


## Screenshots

### Splash Screen
<img align="center" width="300" height="600" src="https://user-images.githubusercontent.com/53400907/168446326-7a515fab-c7c2-404f-b394-59c9067614ff.png">

### Homepage
<img align="left" width="300" height="600" src="https://user-images.githubusercontent.com/53400907/168446095-8957fc96-f5b0-4023-9e1f-645fbf2d1ad6.png">
<img align="center" width="300" height="600" src="https://user-images.githubusercontent.com/53400907/168446166-9e28f48d-da8c-48d5-8cc7-3fd302649fb2.png">

### Create Note Page
<img align="center" width="300" height="600" src="https://user-images.githubusercontent.com/53400907/168446228-bd3b31d0-dea1-4976-a3f5-82146ce7c7aa.png">

### Edit Note Page
<img align="center" width="300" height="600" src="https://user-images.githubusercontent.com/53400907/168446259-922b34ac-ee93-4db1-8663-088745fc008f.png">


### How to run
Step 1: - Use command flutter pub get (Or use tool get pub in Android Studio - recommend)

Step 2: - Use command flutter run ( Or run app by Android Studio - recommend)


